#! /usr/bin/env python3
# coding: utf-8

import os
import sys
from subprocess import check_call, Popen, PIPE, STDOUT, CalledProcessError
import json
import git

WORKSPACE = os.getenv('CI_PROJECT_DIR', '.')
REGISTRY_URL_ROOT = os.getenv('CI_REGISTRY_IMAGE')
BRANCH_NAME = os.getenv('CI_COMMIT_REF_NAME')
IMAGES_ROOTDIR = 'images'
SCRIPTS_ROOTDIR = 'scripts'
TARGET_REMOTE_URL = os.getenv('TARGET_REMOTE_URL')

def get_git_hash_dir(dirpath):
  cmd_1 = ['git', 'ls-files', '-s', dirpath]
  cmd_2 = ['git', 'hash-object', '--stdin']
  p_1 = Popen(cmd_1, stdout=PIPE)
  p_2 = Popen(cmd_2, stdin=p_1.stdout, stdout=PIPE)
  p_1.stdout.close()
  return p_2.communicate()[0].decode('utf-8').strip() # Return stdout

def get_head_commit_sha():
  repo = git.Repo(WORKSPACE)
  return repo.head.commit.hexsha

def commit_and_push_releases_info(images_dict):
  repo = git.Repo(WORKSPACE)
  repo.git.checkout(BRANCH_NAME)
  origin = repo.remote(name='origin')
  if TARGET_REMOTE_URL:
    origin.set_url(TARGET_REMOTE_URL)

  actor = git.Actor('Gitlab CI', 'noreply@gitlab.com')
  file_list = ['releases.json']
  commit_message = "[RELEASE] Update releases.json\nChanges:"
  for img_info in images_dict:
    commit_message = commit_message + "\n  -{}:\t{}".format(img_info['name'], img_info['latest_release'])
  repo.index.add(file_list)
  repo.index.commit(commit_message, author=actor, committer=actor)
  origin.push()

def main():
  # Get all informations
  hcommit = get_head_commit_sha()
  with open('releases.json') as json_file:
    releases_info = json.load(json_file)

  if REGISTRY_URL_ROOT:
    registry_url = '{}/by-commit/'.format(REGISTRY_URL_ROOT)
  else:
    registry_url = ''

  # For each image, do actions
  images_released = []
  for image_info in releases_info['images']:
    image_path = '{}/{}/{}'.format(WORKSPACE, IMAGES_ROOTDIR, image_info['name'])
    build_path = '{}/{}/{}'.format(WORKSPACE, SCRIPTS_ROOTDIR, image_info['build_script'])
    print("[INFO] Processing {}...".format(image_info['name']))
    dir_hash = get_git_hash_dir(image_path)
    print("[INFO] Image: {}\tPrevSHA: {}\tSHA: {}".format(image_info['name'], image_info['latest_dir_sha'], dir_hash))
    if image_info['latest_dir_sha'] == '' or image_info['latest_dir_sha'] != dir_hash:
      build_env = {'IMG_NAME': image_info['name'],
                   'IMG_TAG' : hcommit,
                   'IMG_PATH': image_path,
                   'URL'     : registry_url}
      try:
        check_call(build_path, env=build_env, stderr=STDOUT, shell=True)
      except CalledProcessError as e:
        print("[ERROR] An error occurs. Exiting... ¯\_(ツ)_/¯")
        sys.exit(1)
      image_info['latest_dir_sha'] = dir_hash
      image_info['latest_release'] = hcommit
      images_released.append(image_info['name'])

  # At the end, save modifications into file if at least one image released
  if len(images_released) >= 1:
    with open("releases.json", "w") as json_file:
      json.dump(releases_info, json_file, indent=2)
    commit_and_push_releases_info(releases_info['images'])
    return images_released
  print('No docker image has been modified. Bye !')
  return


if __name__ == '__main__':
  main()

#!/bin/sh

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
URL=${URL:-}
IMG_NAME=${IMG_NAME:-dummy-test}
IMG_TAG=${IMG_TAG:-0.0.1}
IMG_PATH=${IMG_PATH:-${SCRIPTPATH}}

echo "[INFO] Checking docker service..."
CPT=0
while [ ! -S /var/run/docker.sock ];do
  echo -n "[WARN] No docker socket ! (╯°□°)╯︵ ┻━┻..."
  CPT=$(( CPT+1 )) && [ ${CPT} -eq 10 ] && echo 'fail' && exit 1
  echo 'retrying...'
  sleep 1
done
echo "[INFO] Building ${IMG_NAME}:${IMG_TAG} docker image..."
/usr/local/bin/docker build -t "${URL}${IMG_NAME}:${IMG_TAG}" ${IMG_PATH}/.
if [ "z${URL}" != "z" ];then
  echo "[INFO] Pushing docker image to ${URL}..."
  /usr/local/bin/docker push "${URL}${IMG_NAME}:${IMG_TAG}"
fi

What about building automatically Docker images ?
===

What if you want to use only one centralized repository to store all your custom Docker images files ?

Adding a bit of CI with all these images could be tricky then... we've got here a template.

## Structure

Global structure:
```shell
├── images/
│   ├── boostrap-example/
│   ├── dummy-test/
│   └── images-builder/
├── releases.json
└── scripts/
    ├── build.py
    └── build.sh
```

* **images/**: contains definition of all images (automatic or not)
* **releases.json**: store build informations
* **scripts/**: contains all logic call by the CI about the build, release and deployment

## Build info storing

Ideally, the information should be store in a DB (noSQL is sufficient) but this is not the goal here.
Information about automatic builds are stored into a **release.json** file at the root of the repository.
It contains:
 * Name of the image
 * The script used to build and push the image
 * The hash of image folder contents the last time it was processed
 * The SHA1 of the commit the last time it was processed

Each modification on the image will trigger a modification of the **releases.json** and *a new commit to save it*.

